'use strict';

/**
 * Parse client log entry and transform to expected log format.
 * @return {(object|null)} Log entry or null if unable to parse.
*/
module.exports = function parseClientEntry(logEntry) {
    try {
        var json = JSON.parse(logEntry);
        var result = {
            logsource: 'client',
            _data: {},
        };
        for (var prop in json) {
            if (!json.hasOwnProperty(prop)) {
                continue;
            }
            switch (prop) {
                case 'app':
                    result.program = json.app;
                    break;
                case 'ip':
                    result.host = json.ip;
                    break;
                case 'environment':
                    result.env = json.environment;
                    break;
                case 'error':
                    result.type = 'ERROR';
                    result.message = json.error;
                    break;
                case 'message':
                    result.type = 'INFO';
                    result.message = json.message;
                    break;
                case 'timestamp':
                    var timestamp = new Date();
                    timestamp.setTime(parseInt(json.timestamp)*1000);
                    result.timestamp = timestamp.toISOString();
                    break;
                case 'type':
                    break;
                default:
                    result._data[prop] = json[prop];
                    break;
            }
        }
        return result;
    } catch (e) {
        return null;
    }
};
