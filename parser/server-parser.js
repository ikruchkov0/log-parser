'use strict';

/**
 * Map severity to log level
 */
const severityMap = {
    0: 'ERROR',
    1:  'ERROR',
    2: 'ERROR',
    3: 'ERROR',
    4: 'WARNING',
    5: 'INFO',
    6: 'INFO',
    7: 'DEBUG',
};

/**
 * Parse server log
 * @return {(object|null)} Log entry or null if unable to parse it.
*/
module.exports = function parseServerEntry(logEntry) {
    //<${prival}>${version} ${timestamp} ${host} ${app} ${pid} ${mid} ${structured-data} ${message}
    const serverRegExp = /^<(.*?)>(.*?) +(.*?) +(.*?) +(.*?) +(.*?) +(.*?) +\[(.*?)\] +(.*?)$/g;

    try {
        var logEntryParts = logEntry.split(' ');
        var match = serverRegExp.exec(logEntry);
        if (!match || match.length < 10) {
            return null;
        }
        var prival = match[1];
        var version = match[2];
        var timestamp = match[3];
        var host = match[4];
        var app = match[5];
        var pid = match[6];
        var mid = match[7];
        var structuredData = match[8];
        var message = match[9];

        var result = {
            logsource: 'server',
            type: severityMap[getSeverity(prival)],
            timestamp: sanitizeParamValue(timestamp),
            program: sanitizeParamValue(app),
            host:  sanitizeParamValue(host),
            message: sanitizeParamValue(message),
            _data: {
                pid: sanitizeNumberParamValue(pid),
                mid: sanitizeNumberParamValue(mid),
            },
        };

        var structs = parseStructuredData('['+structuredData+']');
        structs.forEach((struct) => {
            populateFromStructData(result, struct.name, struct.data);
        });
        
        return result;
    } catch (e) {
        console.error('Server log parsing error: ', e);
        return null;
    }
};

/**
 * Calculates severity from prival
 * @return {number} Severity calculated from prival.
*/
function getSeverity(prival) {
    prival = parseInt(prival);
    return prival % 8;
}

/**
 * Parse provided structural data in format [struct][struct]...
 * @return {Array} Array of objects.
*/
function parseStructuredData(structuredData) {
    var regex = /\[(.*?)\]/g;
    var match, result = [];
    while (match = regex.exec(structuredData)) {
        var struct = match[1];
        result.push(parseSingleStruct(struct));
    }
    return result;
}

/**
 * Parse single string with structured data between []
 * @return {object} Struct data in format { name: sd-id, data: {param1: value1, ...}}
*/
function parseSingleStruct(struct) {
    var structParts = struct.split(' ');
    if (structParts.length < 2) {
        return null;
    }
    
    var structName = parseStructName(structParts[0]);
    if (!structName) {
        return null;
    }

    var data = {};
    for (var i = 1; i < structParts.length; i++) {
        var param = structParts[i];
        var pair = parseStructParam(param);
        if (!pair) {
            return null;
        }
        data[pair.key] = pair.value;
    }

    return {
        name: structName,
        data: data,
    };
}

/**
 * Parse struct name from string like info@app
 * @return {string} Struct id
*/
function parseStructName(sdId) {
    var sdIdParts = sdId.split('@');
    if (sdIdParts.length != 2) {
        return null;
    }
    return sdIdParts[0];
}

/**
 * Parse params from string like key1="value1" key2="value2"...
 * @return {Array} Array of objects [{key1: value1,...}]
*/
function parseStructParam(param) {
    var match =  /(.*?)="(.*?)"/g.exec(param);
    if (match.length < 3) {
        return null;
    }
    return {
        key: match[1],
        value: match[2],
    };
}

/**
 * Add missing fields to log entry from strcutured data according to rules
*/
function populateFromStructData(result, structName, structuredData) {
    for (var prop in structuredData) {
        if (!structuredData.hasOwnProperty(prop)) {
            continue;
        }
        switch (prop) {
            case 'env':
                result.env = structuredData.env;
                break;
            case 'type':
                break;
            default:
                if (!result._data[structName]) {
                    result._data[structName] = {};
                }
                result._data[structName][prop] = structuredData[prop];
                break;
        }
    }
}

/**
 * Check that parameter is not equal to '-' otherwise returns undefined
 * @return {string} Array of objects [{key1: value1,...}]
*/
function sanitizeParamValue(value) {
    return value !== '-' ? value: undefined;
}

/**
 * Check that parameter is not equal to '-' cast to number otherwise returns undefined
 * @return {Number} Array of objects [{key1: value1,...}]
*/
function sanitizeNumberParamValue(value) {
    var val = sanitizeParamValue(value);
    if (!val) {
        return;
    }
    return parseInt(val);
}