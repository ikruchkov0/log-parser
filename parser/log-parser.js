'use strict';

const { Transform } = require('stream');
const parseClientEntry = require('./client-parser');
const parseServerEntry = require('./server-parser');

const EOL = /[\r\n]+/;

/**
 * Represents mediator stream which transforms incoming logs to expected format
*/
module.exports = class LogParser {
    /**
     * Create a parser
     */
    constructor() {
        this._stream = new Transform({
            transform: (chunk, encoding, callback) => {
                transformLines(chunk, encoding, callback, this._stream);
            }
        });
    }

    /**
     * @return {object} Duplex stream.
     */
    get stream() {
        return this._stream;
    }
}

/**
 * Transforms incoming data from stream to expected log entriess and write back to stream
*/
function transformLines(chunk, encoding, callback, outStream) {
    var lines = chunk.toString().split(EOL);

    var lastTimestamp = '';
    var stackTrace = [];

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        if (line.length == 0) {
            continue;
        }

        var result = parseLine(line);
        if (typeof result !== 'string') {
            if (stackTrace.length > 0) {
                var entry = getStackTraceEntry(stackTrace, lastTimestamp);
                outStream.push(JSON.stringify(entry) + '\n');
                stackTrace = [];
            }
            lastTimestamp = result.timestamp;
            outStream.push(JSON.stringify(result) + '\n');
        } else {
            stackTrace.push(result);
        }
    }
    callback();
}

/**
 * Transforms strack trace to log entry
 * @return {object} Log entry.
*/
function getStackTraceEntry(stackTrace, lastTimestamp) {
    return {
        type: 'ERROR',
        timestamp: lastTimestamp,
        message: stackTrace.join(' | '),
    };
}

/**
 * Parse line from stream
 * @return {(object|string|null)} Log entry or line if unable to parse it.
*/
function parseLine(line) {
    switch (line[0]) {
        case '{':
            return parseClientEntry(line) || line;
        case '<':
            return parseServerEntry(line) || line;
        default:
            return line;
    }
}
