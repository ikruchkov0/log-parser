const Config = require('./config/file-to-file.json');
//const Config = require('./config/tcp-to-tcp.json');
const transport = require('./transport');
const parser = require('./parser');

async function run() {
    const results = await Promise.all([
        transport.createSource(Config.source),
        transport.createTarget(Config.target)
    ]);
    var source = results[0];
    var target = results[1];
    var logParser = new parser.LogParser(source);
    source.stream.pipe(logParser.stream).pipe(target.stream);
}

run();

