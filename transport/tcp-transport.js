var net = require('net');
var util = require('util');
var Abstract = require('./abstract');

class TcpSocketTransport extends Abstract {

    /**
     * Creates transport
     * @param {object} config - config.
     */
    constructor(config) {
        super(config);
        this._socket = new net.Socket()
            .on('error', (err) => {
                console.error('Socket error: ', err);
            })
            .on('close', () => {
                console.log('Tcp transport closed');
            });
    }

    /**
     * Connects to tcp port
     * @return {Promise} Promise with transport object or connection error.
     */
    connect() {
        return new Promise((resolve, reject) => {
            this._socket.connect(this._config.port, this._config.host)
                .on('connect', () => {
                    this._stream = this._socket;
                    resolve(this);
                })
                .on('error', (err) => {
                    reject(err);
                });
        });
    }
}

module.exports = TcpSocketTransport;