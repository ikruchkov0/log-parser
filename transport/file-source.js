var fs = require('fs');
var util = require('util');
var Abstract = require('./abstract');

class FileSource extends Abstract {

  /**
     * Creates file source
     * @param {object} config - config.
     */
  constructor(config) {
    super(config);
    this._stream = fs.createReadStream(this.config.path, {
      flags: 'r',
      encoding: 'utf8',
    });
  }
}


module.exports = FileSource;
