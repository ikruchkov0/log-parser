var FileSource = require('./file-source');
var FileTarget = require('./file-target');
var TcpTransport = require('./tcp-transport');

function createSource(config) {
    return new Promise((resolve, reject) => {
        if (config.type === 'file') {
             resolve(new FileSource(config));
             return;
        }
        if (config.type === 'tcp') {
            return new TcpTransport(config).connect();
        }
        reject(new Error('Unknown source type: ' + config.type));
    });
}

function createTarget(config) {
    return new Promise((resolve, reject) => {
        if (config.type === 'file') {
             resolve(new FileTarget(config));
             return;
        }
        if (config.type === 'tcp') {
            return new TcpTransport(config).connect();
        }
        reject(new Error('Unknown target type: ' + config.type));
    });
}

module.exports = {
    createTarget: createTarget,
    createSource: createSource,
};