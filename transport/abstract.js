class Abstract {

  /**
     * Creates transport
     * @param {object} config - config.
     */
  constructor(config) {
    this._config = config;
    this._stream = null;
  }

  /**
     * @return {object} Config.
     */
  get config() {
    return this._config;
  }

  /**
     * @return {object} Writable, Readable or Duplex stream.
     */
  get stream() {
    return this._stream;
  }
}

module.exports = Abstract;
