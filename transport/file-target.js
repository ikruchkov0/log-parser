var fs = require('fs');
var util = require('util');
var Abstract = require('./abstract');

class FileTarget extends Abstract {

  /**
     * Creates file target
     * @param {object} config - config.
     */
  constructor(config) {
    super(config);
    this._stream = fs.createWriteStream(this.config.path, {
      flags: 'w',
      encoding: 'utf8',
    });
  }

}

module.exports = FileTarget;
