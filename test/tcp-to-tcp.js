var net = require('net');
const assert = require('assert');

var TcpSocketTransport = require('../transport/tcp-transport');

var config = {
    port: 1337,
    host: '127.0.0.1',
};

var clients = [];
var server = net.createServer((socket) => {
    socket.on('data', (data) => {
        clients.forEach((client) => {
            client.write(data);
        });
    });
    clients.push(socket);
});

function cleanup() {
    clients.filter((client) => {
        return !client.destroyed;
    })
    .forEach((client) => {
        client.destroy();
    });
    server.close();
}

var source = new TcpSocketTransport(config);
var target = new TcpSocketTransport(config);
var dataReceived = false;

server.listen(config.port, config.host, async () => {
    var testData = '123';
    try {
        await Promise.all([source.connect(), target.connect()])
        source.stream.on('data', function (data) {
            assert.equal(data.toString(), testData, 'Transfered data is not equal');
            console.log('Tcp to tcp test passed');
            dataReceived = true;
            cleanup();
        });

        target.stream.write(testData);
    } catch (err) {
        assert.fail(err);
    }
});

setTimeout(() => {
    cleanup();
    if (!dataReceived) {
        assert.fail('Timeout');
    }
}, 1000);



