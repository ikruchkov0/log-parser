'use strict';

const assert = require('assert');

const clientParser = require('../parser/client-parser');

var data1 = '{"type": "client", "error": "An application event log entry..", "timestamp": 1518176440, "user_id": 1, "environment": "prod"}';
var expected1 = {
    "logsource": "client",
    "env": "prod",
    "type": "ERROR",
    "timestamp": "2018-02-09T11:40:40.000Z",
    "message": "An application event log entry..",
    "_data": { "user_id": 1 }
};

var data2 = '{"type": "client", "message": "An application event log entry..", "timestamp": 1518176440, "user_id": 1, "environment": "prod", "ip": "127.0.0.1", "app": "client_app"}';
var expected2 = {
    "logsource": "client",
    "program": "client_app",
    "host": "127.0.0.1",
    "env": "prod",
    "type": "INFO",
    "timestamp": "2018-02-09T11:40:40.000Z",
    "message": "An application event log entry..",
    "_data": { "user_id": 1 }
};

var result1 = clientParser(data1);

assert.equal(true, result1 != null, 'client entry has not been parsed');
assert.deepStrictEqual(result1, expected1);

var result2 = clientParser(data2);

assert.equal(true, result2 != null, 'client entry has not been parsed');
assert.deepStrictEqual(result2, expected2);

console.log('Client parser test passed');

