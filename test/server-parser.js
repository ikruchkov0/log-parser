'use strict';

const assert = require('assert');

const serverParser = require('../parser/server-parser');

var data = '<11>1 2018-02-09T12:00:00.003Z 127.0.0.1 app 10000 - [info@app env="prod" type="server" some="data"][data@app some="data"] Error';
var expected = {
    "logsource": "server",
    "program": "app",
    "host": "127.0.0.1",
    "env": "prod",
    "type": "ERROR",
    "timestamp": "2018-02-09T12:00:00.003Z",
    "message": "Error",
    "_data": {
      "pid": 10000,
      "mid": undefined,
      "info": {
        "some": "data"
      },
      "data": {
        "some": "data"
      }
    }
  };


var result = serverParser(data);

assert.equal(true, result != null, 'server entry has not been parsed');
assert.deepStrictEqual(result, expected);

console.log('Server parser test passed');

